
-- Copyright Duyho <duyhungho.work@hcmut.edu.vn>
-- Licensed to the public under the Apache License 2.0.

local fs = require("nixio.fs")

--local fs = require("nixio.fs")
local knownParams = {
	--
	--  Widget
	--	Name
	--	Default(s)
	--	Description
	--	Option(s)
    --{ pubkey,psk,eap-mschapv2,eap-md5,eap-tls,asp-ttls,xauth,xauth-generic,xauth-eap },
    --
	{ "IKE", {
	-- IKE configuration
		{ ListValue,
			"authentication_method",
			{ "pubkey","eap-mschapv2"},
			translate("Set output authentication")},
		{ ListValue,
			"rekey",
			{"yes","no"},
			translate("Rekey or not") },
		{ ListValue,
			"compress",
			{"yes","no"},
			translate("Compress or not") },
		{ Value,
			"dpddelay",
			0,
            translate("set dead pear detection") },
        { Value,
			"dpdtimeout",
			0,
            translate("set time of dead pear detection") },
        { ListValue,
			"dpdaction",
			{ "none","hold","clear","restart" },
            translate("set action for dead pear detection") },
        { ListValue,
			"phase1_proposal",
            {"aes256-sha256-modp1024","aes256-sha512-modp1024","aes256-sha1-modp1024","aes256-sha384-modp1024","aes128-sha256-modp1024","aes128-sha512-modp1024","aes128-sha384-modp1024","aes128-sha1-modp1024","aes128ctr-sha256-modp1024","aes128ctr-sha512-modp1024","aes128ctr-sha384-modp1024","aes128ctr-sha1-modp1024","aes256ctr-sha256-modp1024","aes256ctr-sha512-modp1024","aes256ctr-sha384-modp1024","aes256ctr-sha1-modp1024","3des-sha512-modp1024","3des-sha256-modp1024","3des-sha384-modp1024","3des-sha1-modp1024","3des-md5-modp1024","aes128gcm8-sha512-modp1024","aes256gcm8-sha512-modp1024","aes128gcm16-sha512-modp1024","aes128gcm16-sha512-modp1024"},
			translate("set cipher suite/ refer: strongswan cipher suite") },
		{ Value,
			"remotegateway",
			"%any",
			translate("host name or ip address") },
		{ Value,
			"localgateway",
			"%any",
			translate("host name or ip address") },
		{ Value,
			"leftcert",
			"/etc/ipsec.d/certs/vpn-server-cert.pem",
			translate("Directories for left cert") },
		{ Value,
			"leftkey",
			"/etc/ipsec.d/private/vpn-server-key.pem",
			translate("Directories for left key") },
		{ ListValue,
			"leftsendcert",
			{"always","never"},
			translate("Set mode for router to send certificate") },
		{ Value,
			"rightsendcert",
			{"always","never"},
            translate("Set mode for client to send certificate") },
		{ Value,
			"local_identifier",
            "0.0.0.0",
			translate("Host name or Wan Ip of router which is used for client to get access to server") },
		{ Value,
			"remote_identifier",
            "0.0.0.0",
            translate("Host name /IP of client") },
        } },

	{ "ESP", {
	-- ESP configuration

		{ Value,
			"local_subnet",
			"0.0.0.0/0",
			translate("subnet of virtual Lan subnet that server supports for client") },
		{ Value,
			"rightsourceip",
            "0.0.0.0/24",
			translate("Virtual IP for client in server site") },
        { ListValue,
			"phase2_proposal",
            {"aes256-sha256-modp1024","aes256-sha512-modp1024","aes256-sha1-modp1024","aes256-sha384-modp1024","aes128-sha256-modp1024","aes128-sha512-modp1024","aes128-sha384-modp1024","aes128-sha1-modp1024","aes128ctr-sha256-modp1024","aes128ctr-sha512-modp1024","aes128ctr-sha384-modp1024","aes128ctr-sha1-modp1024","aes256ctr-sha256-modp1024","aes256ctr-sha512-modp1024","aes256ctr-sha384-modp1024","aes256ctr-sha1-modp1024","3des-sha512-modp1024","3des-sha256-modp1024","3des-sha384-modp1024","3des-sha1-modp1024","3des-md5-modp1024","aes128gcm8-sha512-modp1024","aes256gcm8-sha512-modp1024","aes128gcm16-sha512-modp1024","aes128gcm16-sha512-modp1024"},
			translate("set cipher suite/ refer: strongswan cipher suite") },
	} },
	{ "USER", {
	-- User and password configuration
	{ Value,
	"username1",
	0,
	translate("Set up username1") },
    { Value,
	"password1",
	0,
	translate("Set up password1") },
    { Value,
	"username2",
	0,
	translate("Set up username2") },
    { Value,
	"password2",
	0,
	translate("Set up password2") },
	{ Value,
	"username3",
	0,
	translate("Set up username3") },
    { Value,
	"password3",
	0,
	translate("Set up password3") },
    { Value,
	"username4",
	0,
	translate("Set up username4") },
    { Value,
	"password4",
	0,
	translate("Set up password4") },
	{ Value,
	"username5",
	0,
	translate("Set up username5") },
    { Value,
	"password5",
	0,
	translate("Set up username5") },
    { Value,
	"username6",
	0,
	translate("Set up username6") },
    { Value,
	"password6",
	0,
	translate("Set up password6") },
	{ Value,
	"username7",
	0,
	translate("Set up username7") },
    { Value,
	"password7",
	0,
	translate("Set up username7") },
    { Value,
	"username8",
	0,
	translate("Set up username8") },
    { Value,
	"password8",
	0,
	translate("Set up password8") },
	{ Value,
	"username9",
	0,
	translate("Set up username9") },
    { Value,
	"password9",
	0,
	translate("Set up username9") },
    { Value,
	"username10",
	0,
	translate("Set up username10") },
    { Value,
	"password10",
	0,
	translate("Set up password10") },
	} }
}

local cts = { }
local params = { }
--local arg = {}
local m = Map("ipsec")

m.redirect = luci.dispatcher.build_url("admin", "vpn", "ipsecvpn2")
m.apply_on_parse = true

local p = m:section( SimpleSection )
local cfg = p:option(DummyValue, "config")

p.template = "ipsec-module2/pageswitch"
p.mode     = "advanced"
p.instance = arg[1]

--p.instance = "esped"
p.category = arg[2] or "IKE"
--translate(p.category)

for _, c in ipairs(knownParams) do
	cts[#cts+1] = c[1]
	if c[1] == p.category then params = c[2] end
end

-- function p.cfgvalue(self,option)
--     return translate(option)
-- end

p.categories = cts
--p.extedit = luci.dispatcher.build_url("admin", "vpn", "ipsecvpn", "advanced",p.instance,p.category)
local s = m:section(
	NamedSection, p.instance, "client2server",
	translate("%s" % p.category)
)

-- end
-- local Named =s:option(DummyValue,"_name",translate("Name"))
-- function Named.cfgvalue(self,section)
--     return translate("%s" % p.instance)
-- end
s.title     = translate("%s" % p.category)
s.addremove = false
s.anonymous = true


for _, option in ipairs(params) do
	local o = s:option(
		option[1], option[2],
		option[2], option[4]
	)

	o.optional = true

	if option[1] == DummyValue then
		o.value = option[3]
	elseif option[1] == FileUpload then

		function o.cfgvalue(self, section)
			local cfg_val = AbstractValue.cfgvalue(self, section)

			if cfg_val then
				return cfg_val
			end
		end

		function o.formvalue(self, section)
			local sel_val = AbstractValue.formvalue(self, section)
			local txt_val = luci.http.formvalue("cbid."..self.map.config.."."..section.."."..self.option..".textbox")

			if sel_val and sel_val ~= "" then
				return sel_val
			end

			if txt_val and txt_val ~= "" then
				return txt_val
			end
		end

		function o.remove(self, section)
			local cfg_val = AbstractValue.cfgvalue(self, section)
			local txt_val = luci.http.formvalue("cbid."..self.map.config.."."..section.."."..self.option..".textbox")
			if cfg_val and fs.access(cfg_val) and txt_val == "" then
				fs.unlink(cfg_val)
			end
			return AbstractValue.remove(self, section)
		end
	elseif option[1] == Flag then
		o.default = nil
	else
		if option[1] == DynamicList then
			function o.cfgvalue(...)
				local val = AbstractValue.cfgvalue(...)
				return ( val and type(val) ~= "table" ) and { val } or val
			end
		end

		if type(option[3]) == "table" then
			if o.optional then o:value("", "-- remove --") end
			for _, v in ipairs(option[3]) do
				v = tostring(v)
				o:value(v)
			end
			o.default = tostring(option[3][1])
		else
			o.default = tostring(option[3])
		end
	end

	for i=5,#option do
		if type(option[i]) == "table" then
			o:depends(option[i])
		end
	end
end

return m
