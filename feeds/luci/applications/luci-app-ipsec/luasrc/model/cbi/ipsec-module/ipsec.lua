-- CBI file: describe the structure of an UCI config file and the
-- resulting HTML from to be evaluated by the CBI parser
-- Licensed to the public under the Apache License 2.0.
-- Low-level and high-level filesystem manipulation library.

local fs  = require "nixio.fs"
local sys = require "luci.sys"
local uci = require "luci.model.uci".cursor()
local uci1 = require "luci.model.uci".Cursor
local testfullps = sys.exec("ps --help 2>&1 | grep BusyBox") --check which ps do we have
local psstring = (string.len(testfullps)>0) and  "ps w" or  "ps axfw" --set command we use to get pid
local mp = Map("ipsec", translate("IPSec Site-to-Site")) 

local st  = mp:section(SimpleSection)
st.template = "ipsec-module/modeswitch"
st.mode = "site2site"
local s = mp:section(TypedSection,"site2site",translate("IPSec Connections"),translate("Below is a list of configured IPsecVPN connection and their current state"))

s.anonymous = true
s.template = "cbi/tblsection"
s.template_addremove = "ipsec-module/cbi-select-input-add"
s.addremove = true
s.add_select_options = { }
-------------------------------------------------------------
local cfg = s:option(DummyValue, "config")

function cfg.cfgvalue(self, section)
	local file_cfg = self.map:get(section, "config")
	if file_cfg then
		s.extedit = luci.dispatcher.build_url("admin", "vpn", "ipsecvpn","file","%s")
	else
		s.extedit = luci.dispatcher.build_url("admin", "vpn", "ipsecvpn", "advanced","%s")
	end
end 

uci:load("ipsec")
uci:foreach( "ipsec", "site2site",
	function(section)
		s.add_select_options[section['.name']] =
			section['_description'] or section['.name']
	end
)
--Grabs PID number- process id 
function s.getPID(section) -- Universal function which returns valid pid # or nil
	local pid = sys.exec("%s | grep -w '%s'" % { psstring, section })
	if pid and #pid > 0 then
		return tonumber(pid:match("^%s*(%d+)"))
		--return nil
	else
		return nil
    end
end
---------------------------------------------------------------------------
function s.parse(self, section)
	local recipe = luci.http.formvalue(
		luci.cbi.CREATE_PREFIX .. self.config .. "." ..
		self.sectiontype .. ".select"
	)
	if recipe and not s.add_select_options[recipe] then
		self.invalid_cts = true
	else
		TypedSection.parse( self, section )
	end
end
--------------------------------------------------------------------------------
function s.create(self, name)
	local name = luci.http.formvalue(
		luci.cbi.CREATE_PREFIX .. self.config .. "." ..
		self.sectiontype .. ".text"
	)
	if #name > 3 and not name:match("[^a-zA-Z0-9_]") then
		local s = uci:section("ipsec", "site2site", name)
		if s then
			uci:set("ipsec",name,"enabled","1")
			uci:set("ipsec",name,"authentication_method","psk")
			uci:set("ipsec",name,"pre_shared_key","123456789")
			uci:set("ipsec",name,"keyingtries","0")
			uci:set("ipsec",name,"ikelifetime","1h")
			uci:set("ipsec",name,"lifetime","8h")
			uci:set("ipsec",name,"dpddelay","10s")
			uci:set("ipsec",name,"dpdaction","restart")
			uci:set("ipsec",name,"dpdtimeout","20s")
			uci:set("ipsec",name,"remotegateway","0.0.0.0")
			uci:set("ipsec",name,"localgateway","0.0.0.0")
			uci:set_list("ipsec",name,"p1_proposal","pre_g2_aes_sha1")
			uci:set_list("ipsec",name,"tunnel","71_to_101")
			uci:save("ipsec")
			uci:commit("ipsec")
		end
	elseif #name > 0 then
		self.invalid_cts = true
	end
	return 0
end
-- ----------------------------------------------------------------------------
s.redirect = luci.dispatcher.build_url(
	"admin"
)
function s.remove(self,section)
	uci:delete("ipsec", section)
	uci:save("ipsec")
	uci:commit("ipsec")
	return 0
end

--------------------------------------------------------------------------------
local Named =s:option(DummyValue,"_name",translate("Name Connection"))
function Named.cfgvalue(self,section)
    return translate(section)
end
---------------------------------------------------------------------
s:option( Flag, "enabled", translate("Enabled") )

local active = s:option( DummyValue, "_active", translate("Status") )
function active.cfgvalue(self, section)
	local ret1 = sys.exec("ipsec status | grep %s" % section)
	local ret2 = sys.exec("grep -wi \"failed\" /www/ggg.txt ")
    if (ret1 ~= "") then
	--if ret1 ~= "" then
		return translate("Succeed")
	elseif ((ret1 == "") and (ret2 ~= "")) then
	    return translate("Failed to connect")
	else
		return translate("No")
	end
end

local updown = s:option( Button, "_updown", translate("Start/Stop") )

updown._state = false
local url={'/admin/vpn/ipsecvpn'}
updown.redirect = url
local clock = os.clock
function updown.sleep(self , n)  -- seconds
  local t0 = clock()
  while clock() - t0 <= n do end
end
function updown.cbid(self, section)
	local ret = sys.exec("ipsec status | grep %s" % section)
	if ret ~= "" then
		self._state = true
	else
		self._state = false
	end
	self.option = self._state and "stop" or "start"
	return AbstractValue.cbid(self, section)
end
function updown.cfgvalue(self, section)
	self.title = self._state and "stop" or "start"
	self.inputstyle = self._state and "reset" or "reload"
end
function updown.write(self, section, value)
	local result
	if self.option == "stop" then
		result = sys.call("ipsec down %s > ggg.txt" % section)
	else
		result = sys.call("iptables -t nat -I POSTROUTING -m policy --pol ipsec --dir out -j ACCEPT > ggg.txt")
		result = sys.call("/etc/init.d/ipsec start > ggg.txt")
		result = sys.call("ipsec up %s > ggg.txt" % section )
	end
	luci.http.redirect( '/cgi-bin/luci/admin/vpn/ipsecvpn' )
end

function mp.on_after_commit(self,map)
	sys.call('/etc/init.d/ipsec reload')
end

function mp.on_after_apply(self,map)
	sys.call('/etc/init.d/ipsec restart')
end

return mp

